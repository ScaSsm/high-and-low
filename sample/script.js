/*
* TECH.S 2020 プログラミング言語体験入学
* High and Lowのプログラムです。
* htmlの表示/非表示、ボタン操作に関する部分は予め書いてあります。
* 処理の部分をコーディングしてみましょう。
*/

//左カード
var leftCard = $(".left-card");
//右カード
var rightCard = $(".right-card");
//右カードの数字（表には見せない）
var rightCardNum;
//正解カウンター
var correctCounter;

//お作法的な部分
$(function(){
    //スタートボタンを押した時
    $(".start-btn").click(function(){
        //ゲーム起動
        start();
    });

    //High or Low 選択時
    $(".select-btn").click(function(){
        //正解表示
        rightCard.text(rightCardNum);
        
        //正誤判定
        //正解の場合
        if($(this).data("val") == highOrLow()){
            //正解カウンターup
            correctCounter +=1;
            //正解表示
            $(".message").text("正解！ 連続正解数：" + correctCounter);
            //ボタン切り替え
            $(".select-btn-area").addClass("hidden");
            $(".next-btn-area").removeClass("hidden");
        //不正解の場合
        }else{
            //ゲーム終了
            //不正解表示 文字色変更
            $(".message").text("不正解 連続正解数：" + correctCounter);
            $(".message").removeClass("text-warning").addClass("text-danger");
            $(".select-btn-area").addClass("hidden");
            $(".restart-btn-area").removeClass("hidden");
        }
    });

    //次へボタンを押した時
    $(".next-btn").click(function(){
        //メッセージを戻す
        $(".message").text("High or Low?");

        //左カードの数字を右カードの数字に置き換える
        leftCard.text(rightCardNum);

        //右カード再抽選
        rightCardNum = setRigthCard(leftCard.text());
        rightCard.text("?");
        //High or Lowボタン再表示
        $(".next-btn-area").addClass("hidden");
        $(".select-btn-area").removeClass("hidden");
    });
    
    //もう一度遊ぶボタンを押した時
    $(".restart-btn").click(function(){
        //ゲーム起動
        start();
    });    
});

/**
 * ゲーム起動
 */
function start(){
    //ゲーム初期化
    initGame();
    //画面切り替え
    $(".start").addClass("hidden");
    $(".game-area").removeClass("hidden");
}

/**
 * ゲーム初期化
 */
function initGame(){
    //正解カウンターリセット
    correctCounter = 0;
    //左のカード数字決定
    leftCard.text(draw());
    //右カードの数字決定（表示は"?"にするので、値だけ持たせておく）
    rightCard.text("?");
    rightCardNum = setRigthCard(leftCard.text());

    //メッセージ設定
    $(".message").text("High or Low?");
    $(".message").removeClass("text-danger").addClass("text-warning");
    //ボタン表示設定
    $(".select-btn-area").removeClass("hidden");
    $(".next-btn-area").addClass("hidden");
    $(".restart-btn-area").addClass("hidden");
}

/**
 * 右カードの数字決定
 */
function setRigthCard(leftCardNum){
    //数字をランダムに取得
    var tmpNum = draw();
    //左カードとかぶらないようにする
    //左の数字と同じだった場合再抽選
    while(tmpNum == leftCardNum){
        tmpNum = draw();
    }
    return tmpNum;
}

/**
 * 数字の抽選
 */
function draw(){
    //1～13の数字をランダムで取得
    var min = 1;
    var max = 13;
    var num = Math.floor( Math.random() * (max + 1 - min) ) + min ;

    //返却
    return num;
}

/**
 * High or Low判定
 * カードを比較し"high"または"low"を返却します。
 * @return string
 */
function highOrLow(){
    //数字を比較する
    //？のほうが大きい
    if(leftCard.text() < rightCardNum){
        return "high";
    //？の方が小さい
    }else{
        return "low";
    }
}